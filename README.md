# Arm Corstone-1000 Applications

---

__🚨Disclaimer:__

*_Arm reference solutions are Arm public example software projects that track and pull upstream components, incorporating their respective security fixes published over time._*
*_Users of these solutions are responsible for ensuring that the components they use contain all the required security fixes, if and when they deploy a product derived from Arm reference solutions._*

---

Welcome to the Arm Corstone-1000 Applications repository!

This repository contains software applications designed to test the external system of the [Corstone-1000 Arm pre-integrated IP subsystems and system IP][corstone-1000-system-ip].
It is designed to work with the Corstone-1000 reference platform software stack.

For detailed information about the Corstone-1000 software stack, including usage of the applications
in this repository, refer to the [official Corstone-1000 software stack User Guide][corstone-1000-sw-stack-user-guide].


[corstone-1000-system-ip]: https://developer.arm.com/Processors/Corstone-1000
[corstone-1000-sw-stack-user-guide]: https://corstone1000.docs.arm.com/en/corstone1000-2023.06/user-guide.html#running-the-external-system-test-command-systems-comms-tests
